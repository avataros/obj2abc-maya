import maya.cmds as cmds
import os

def get_obj_files(folder_path):
    obj_files = []
    for filename in sorted(os.listdir(folder_path)):
        if filename.endswith('.obj'):
            obj_files.append( os.path.join(folder_path, filename) )
    return obj_files

def delete_all(mesh_list):
    cmds.select(mesh_list, )
    mesh_transforms = cmds.listRelatives(mesh_list, parent=True, fullPath=True)
    # TODO: this is currently selecting lots of stuff it shouldn't but doesnt explode
    cmds.select(mesh_transforms, add=True)
    cmds.delete()

def convert_objs_to_abc(obj_folder, output_abc_file):
    obj_files = get_obj_files(obj_folder)
    for obj_file in obj_files:
        cmds.file(obj_file,i=True,dns=True)

    imported_meshes = cmds.ls(geometry=True)
    imported_meshes = sorted(imported_meshes)
    # The first imported frame will not have numbers in the name and will be sorted to the end
    
    # The last selected node is the target mesh for the blend shape
    cmds.select(imported_meshes)
    blendshape_node = cmds.blendShape(name="frames", origin="world")[0]
    
    # Delete the other imported meshes
    delete_all(imported_meshes[:-1])
    
    # get a list of the blendshape attrs
    blendshape_attrs = cmds.listAttr(blendshape_node, multi=True)
    blend_frames = []
    for attr in blendshape_attrs:
        if "Shape" in attr:
            a = blendshape_node + "." + attr
            blend_frames.append(a)

    # Set the blendshape attrs based on the frame number
    start_frame = 1
    last_frame = len(blend_frames)
    for f in range(0, len(blend_frames)):
        frame = f + 1
        active_blendshape = blend_frames[f]
        # print("Frame", frame, blend_frames[f])
        for a in blend_frames:
            cmds.select(a, r=True)
            if a == active_blendshape:
                cmds.setKeyframe( v=1, t=frame)
            else:
                cmds.setKeyframe( v=0, t=frame)
                
    # Export the mesh to an abc file
    mesh = cmds.ls(tr=True, v=True)[0]
    export_cmd = "-frameRange " + str(start_frame) + " " + str(last_frame) + " -uvWrite -worldSpace -dataFormat ogawa -root |" + mesh + " -file " + output_abc_file
    cmds.AbcExport ( j = export_cmd )  

def set_fps(fps = "30"):
    unit_names = ["game", "film", "pal", "ntsc", "show", "palf", "ntscf", "23.976fps", "29.97fps", "29.97df", "47.952fps", "59.94fps", "44100fps", "48000fps"]
    fps_vals = ["15", "24", "25", "30", "48", "50", "60", "23.976", "29.97", "29.97df", "47.952", "59.94", "44100", "48000"]
    
    try:
        index = fps_vals.index(fps)
        unit_name = unit_names[index]
    except:
        print("Invalid fps value", fps)
        print("Valid values are", fps_vals)
        sys.exit(1)
        
    cmds.currentUnit( time=unit_name )

if __name__ == "__main__":
    import sys
    import maya.standalone as std
    std.initialize(name='python')
    
    print(sys.argv)
    
    if len(sys.argv) < 3:
        print("Usage: maya_obj2abc.py <obj_folder> <output_abc_file> <fps> = 30")
        sys.exit(1)
    else:
        obj_folder = sys.argv[1]
        output_abc_file = sys.argv[2]
        if len(sys.argv) > 3:
            fps = sys.argv[3]
        else:
            fps = "30"
        set_fps(fps)
        obj_folder = os.path.abspath(obj_folder)
        output_abc_file = os.path.abspath(output_abc_file)
        print("Converting", obj_folder, "to", output_abc_file)
        convert_objs_to_abc(obj_folder, output_abc_file)

    
