# Obj2Abc Maya

## Description

Takes a folder of obj files and converts them to an alembic file. Just like Maya it is super inefficient and might take a long time to process.

## Usage

```cmd
{path to mayapy.exe} {path to maya_obj2abc.py} {path to folder of obj files} {path to output abc file} { optional: fps}
```

e.g. `"C:\Program Files\Autodesk\Maya2020\bin\mayapy.exe" "%~dp0maya_obj2abc.py" "objs" "output.abc" 24`

